/**
 * NashTech VietNam
 * 
 * Copyright @ 2016 Trung Dang
 */

'use strict';
// enable logging
process.env.NODE_DEBUG = 'botbuilder';

const config = require('./config');
let db = require('./lib/database')();
const botBootstraper = require('./lib/bot');
let dbConnection = {};

let bot = botBootstraper(config);
require('./lib/dialog')(dbConnection, config, bot);