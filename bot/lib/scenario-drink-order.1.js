/**
 * Drink Order Scenario
 * 
 * NashTech VietNam
 * Copyright @ 2016 Trung Dang 
 */

'use strict';
const builder = require('botbuilder');
const _ = require('lodash');
let fakeOrder = {
};

function drinkStepBegin(session, args) {
    let availableOptions = [
        'Absolutely, these are what we have.',
        'Of course, please see our drinks list.',
        'Happy to help, please see our drinks menu.',
        'Happy to be of service. Please see our drinks menu.',
        'Kindly browse our selection here.'
    ];
    drinkResetDialogData(session);
    session.send(_.sample(availableOptions));

    // 
    let msg = new builder.Message(session);
    let userId = session.message.address.user.id;

    let attachments = [];
    for (let productKey in global.Config.products) {
        let product = global.Config.products[productKey];

        let attach = new builder.HeroCard(session)
            .title(product.name)
            .text(product.desc)
            .images([
                new builder.CardImage(session).url(product.image)
            ])
            .buttons([
                new builder.CardAction(session)
                    .type('imBack')
                    .title('Reserve')
                    .value(product.imBack)
                //.value(`${global.Config.RESERVE_DRINKS_URL}/${product.id}/${userId}`)
            ]
            );
        attachments.push(attach);
    }
    msg.attachments(attachments);
    msg.attachmentLayout(builder.AttachmentLayout.carousel);
    session.send(msg);
}

function drinkStepSelect(session, args) {
    let entityDrinkType = builder.EntityRecognizer.findEntity(args.entities, 'drink-type');
    let entityNumber = builder.EntityRecognizer.findEntity(args.entities, 'builtin.number');

    function drinkNotRecognize() {
        // could not recognize this answer
        session.send('Sorry, but we dont have that kind of drink in our menu');
        session.endDialog();
    }

    function recognizeDrink() {
        function recognizedDrink(drink) {
            let availableOptions = [
                'How many would you like to reserve?',
                'Let me know how many you’d like.'
            ];

            session.send(`Ok, ${drink.name}`);
            // session.send(_.sample(availableOptions));
            builder.Prompts.text(session, _.sample(availableOptions));
            fakeOrder.drinkId = drink.id;
            fakeOrder.drinkQuantities = 0;
        }

        let drinkName = entityDrinkType.entity.toLowerCase();
        let result = _.filter(global.Config.products, (v) => {
            return v.name.toLowerCase() === drinkName;
        });

        if (result.length === 0) {
            drinkNotRecognize();
        }
        else {
            recognizedDrink(result[0]);
        }
    }

    function recognizeQuantities(entity) {
        let quantities = entity.entity;
        // TODO: should convert quantities to number
        fakeOrder.drinkQuantities = quantities;

        builder.Prompts.text(session, 'Do you have any special comments?');
    }

    if (entityDrinkType) {
        recognizeDrink();
    }
    else if (entityNumber && fakeOrder.drinkId && fakeOrder.drinkId > 0) {
        recognizeQuantities(entityNumber);
    }
    else if (fakeOrder.drinkId > 0 && fakeOrder.drinkQuantities.length != 0) {
        fakeOrder.comments = session.message.text;
        drinkShowReceipt(session);
    }
    else {
        drinkNotRecognize();
    }
}

function drinkStepEnd(session, args) {
    session.endDialog();
}

function drinkResetDialogData(session) {
    fakeOrder.drinkId = 0;
    fakeOrder.drinkQuantities = 0;
    fakeOrder.comments = '';
}

function drinkStepSelectOther(session, args) {
    drinkResetDialogData(session);
    sesson.send('Thanks, waiter will contact you for more information');
}

function drinkStepNoComments(session, args) {
    if (fakeOrder.drinkId > 0 && fakeOrder.drinkQuantities.length != 0) {
        fakeOrder.comments = '';
        drinkShowReceipt(session);
    }
    else {
        session.send('Sorry, I dont understand for what you mean');
        session.endDialog();
    }
}

function drinkShowReceipt(session) {
    session.send('Receipt here');
}

// function registerReserveDrinksCallback(dialog) {
//     global.server.get('/reserve/drinks/:id/:userId', (req, res, next) => {
//         res.send('Hello')
//     });
// }

function scenarioDrinkOrder(recognizer) {
    let dialog = new builder.IntentDialog({
        recognizers: [recognizer]
        //, recognizeMode: builder.RecognizeMode.onBegin
    });
    dialog['_internalName'] = 'scenarioDrinkOrder';
    // registerReserveDrinksCallback(dialog);

    dialog
        .matches('order-drink', drinkStepBegin)
        .matches('drink-select', drinkStepSelect)
        .matches('drink-other', drinkStepSelectOther)
        .matches('drink-no-comments', drinkStepNoComments)
        .onBegin(drinkResetDialogData)
        .onDefault(function (session) {
            session.send("I dont understand for what you mean");
        });

    return dialog;
}

module.exports = scenarioDrinkOrder;