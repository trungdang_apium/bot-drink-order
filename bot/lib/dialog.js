/**
 * Dialog / intents wrapper
 * 
 * NashTech VietNam
 * Copyright @ 2016 Trung Dang 
 */

'use strict';
const builder = require('botbuilder');
const _ = require('lodash');
let dbConnection;
let recognizer;
const scenarioDrinkOrder = require('./scenario-drink-order');
const sessionRandomId = _.random(1,99999);

/**
 * Handle bot events such as contact added, start group conversations
 * 
 * @param {any} bot
 */
function eventHandling(bot) {
    console.log(`Random Id = ${sessionRandomId}`);
    bot.use(builder.Middleware.sendTyping());
    bot.use({
        botbuilder: function (session, next) {
            var cur = session.sessionState.randomId || 0.0;
            if (session.sessionState.callstack.length && cur !== sessionRandomId) {
                session.userData.orders = [];
                session.userData.currentOrder = {};
                session.endConversation();
                session.sessionState.randomId = sessionRandomId;
            }
            else {
                session.sessionState.randomId = sessionRandomId;
            }
            next();
        }
    });
    bot.use(builder.Middleware.dialogVersion({ version: 1.0, resetCommand: /^reset/i }));
    //=========================================================
    // Bots Global Actions
    //=========================================================

//Hello, welcome to the NashTech bar.
    bot.endConversationAction('startover', 'Hello, welcome to the NashTech bar.', { 
        matches: /^start over/i 
    });
    bot.beginDialogAction('help', '/help', { matches: /^help/i });

    bot.on('contactRelationUpdate', function (message) {
        if (message.action === 'add') {
            let availableRet = [
                "Hello %s, welcome to the NashTech BAR."
            ];

            var name = message.user ? message.user.name : null;
            console.log(`Adding user ${message.user}`);
            var reply = new builder.Message()
                .address(message.address)
                .text(_.sample(availableRet), name || 'there');

            bot.send(reply);
        } else {
            // delete their data
            console.log(`Removing user ${message.user}`);
        }
    });

    bot.on('deleteUserData', function (message) {
        // User asked to delete their data
        console.log(`User added to remove their data ${message.user}`);
    });

    // setTimeout(function () {
    //     let userGuidId = '29:1ZPSQZsd5ZJtSnsh4EO-nvfiy0f7oJDaJoPWWeDkkVUY';
    //     var m = new builder.Message()
    //         .address({
    //             // "id": "Fx1OejOHGDN",
    //             "channelId": "skype",
    //             "serviceUrl": "https://skype.botframework.com",
    //             "useAuth": true,
    //             "user": {
    //                 "id": userGuidId
    //             },
    //             "conversation": {
    //                 "id": userGuidId
    //             },
    //             "bot": {
    //                 // "id": "28:0c6f88e6-0335-4b83-b8df-d0fccd97ea60",
    //                 "id": bot.connectors['*'].settings.appId
    //             }
    //         })
    //         .text('this is a test');

    //     bot.send(m);

    // }, 3000);
}

function registerDefaultIntent(dialog) {
    dialog.matches('order-drink', function (session, args) {
        session.beginDialog('drink-order');
    });

    dialog.matches('welcome', function (session, args) {
        let availableOptions = [
            'Hello %s, welcome to the NashTech BAR.'
        ];
         var name = session.message.user ? session.message.user.name : null;
        session.send(_.replace(_.sample(availableOptions), '%s', name));
    });

    dialog.onDefault(function (session, args) {
        session.send('Sorry but I dont understand what you mean.');
    });
}

/**
 * Try to register dialog intents
 * 
 * @param {any} config
 * @param {any} bot
 */
module.exports = function (dbConnection, config, bot) {
    eventHandling(bot);

    let model = config.LUIS_SERVICE_URL;
    recognizer = new builder.LuisRecognizer(model);

    let dialog = new builder.IntentDialog({ recognizers: [recognizer] });
    dialog['_internalName'] = 'mainDialog';

    bot.dialog('/', dialog);

    bot.dialog('/help', [
        function (session) {
            session.endDialog("Global commands that are available anytime:\n\n* drink - Show the drinks menu.\n* goodbye - End this conversation.\n* help - Displays these commands.");
        }
    ]);

    registerDefaultIntent(dialog);
    scenarioDrinkOrder(recognizer, bot, dialog);
};