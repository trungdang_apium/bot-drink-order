/**
 * Drink Order Scenario
 * 
 * NashTech VietNam
 * Copyright @ 2016 Trung Dang 
 */

'use strict';
const builder = require('botbuilder');
const _ = require('lodash');

/**
 * Step 1: tell What whould you want to drink?
 * Then show the categories
 */
function drinkStepShowCategories(session, args) {
    let availableOptions = [
        'Absolutely, these are what we have.',
        'Of course, please see our drinks list.',
        'Happy to help, please see our drinks menu.',
        'Happy to be of service. Please see our drinks menu.',
        'Kindly browse our selection here.',
        'What would you want to drink?'
    ];
    drinkResetDialogData(session);
    session.send(_.sample(availableOptions));

    // show categories 
    let msg = new builder.Message(session);
    let userId = session.message.address.user.id;

    let attachments = [];
    for (let catId in global.Config.productCategories) {
        let cat = global.Config.productCategories[catId];

        let attach = new builder.HeroCard(session)
            .title(cat.name)
            .text(cat.desc)
            .images([
                new builder.CardImage(session).url(cat.image)
            ])
            .buttons([
                new builder.CardAction(session)
                    .type('imBack')
                    .title('Select')
                    .value(cat.imBack)
                //.value(`${global.Config.RESERVE_DRINKS_URL}/${product.id}/${userId}`)
            ]
            );
        attachments.push(attach);
    }
    msg.attachments(attachments);
    msg.attachmentLayout(builder.AttachmentLayout.carousel);
    session.send(msg);
}

function drinkStepShowProducts(session, args, products) {
    let availableOptions = [
        'Absolutely, these are what we have.',
        'Of course, please see our drinks list.',
        'Happy to help, please see our drinks menu.',
        'Happy to be of service. Please see our drinks menu.',
        'Kindly browse our selection here.'
    ];
    drinkResetDialogData(session);
    session.send(_.sample(availableOptions));

    // 
    let msg = new builder.Message(session);
    let userId = session.message.address.user.id;

    let attachments = [];
    for (let productKey in products) {
        let product = products[productKey];

        let attach = new builder.HeroCard(session)
            .title(product.name)
            .text(product.desc)
            .images([
                new builder.CardImage(session).url(product.image)
            ])
            .buttons([
                new builder.CardAction(session)
                    .type('imBack')
                    .title('Reserve')
                    .value(product.imBack)
                //.value(`${global.Config.RESERVE_DRINKS_URL}/${product.id}/${userId}`)
            ]
            );
        if (product.price > 0) {
            attach.subtitle(`£${product.price}`);
        }

        attachments.push(attach);
    }
    msg.attachments(attachments);
    msg.attachmentLayout(builder.AttachmentLayout.carousel);
    session.send(msg);
}

function drinkStepSelect(session, args) {
    let entityDrinkType = builder.EntityRecognizer.findEntity(args.entities, 'drink-type');

    function drinkNotRecognize() {
        // could not recognize this answer
        session.send('Sorry, but we dont have that kind of drink in our menu');
        //session.endDialog();
    }

    /**
     * Try to recognize the input1 and input2 name from the collection
     */
    function recognize(collection, input1, input2) {
        let result = _.filter(collection, (v) => {
            let rule1 = v.name.toLowerCase() === input1;
            let rule2 = v.hasOwnProperty('regex') && v.regex.test(input2);

            return rule1 || rule2;
        });
        return result;
    }

    function recognizedDrink(drink) {
        //session.send(`Ok, ${drink.name}`);
        session.userData.currentOrder = session.userData.currentOrder || {};
        session.userData.currentOrder.drinkId = drink.id;
        session.userData.currentOrder.drinkQuantities = 0;
        session.userData.currentOrder.drinkName = drink.name;
        session.beginDialog('orderDrinkSelectQuantities');
    }

    let drinkName = '';
    if (entityDrinkType) {
        drinkName = entityDrinkType.entity.toLowerCase();
    }
    let testProducts = recognize(global.Config.products, drinkName, session.message.text);
    let testCategories = recognize(global.Config.productCategories, drinkName, session.message.text);

    if (testCategories.length > 0 && testProducts.length == 0) {
        let cat = testCategories[0];
        let products = _.filter(global.Config.products, (v) => {
            return cat.id == v.catId;
        });

        // we found a category
        drinkStepShowProducts(session, args, products);
    }
    else if (testProducts.length > 0) {
        recognizedDrink(testProducts[0]);
    }
    else {
        drinkNotRecognize();
    }
}

function drinkStepEnd(session, args) {
    session.endDialog();
}

function drinkResetDialogData(session, args, next) {
    session.userData.orders = session.userData.orders || [];
    session.userData.currentOrder = {
        drinkId: 0,
        drinkQuantities: 0,
        comments: ''
    };
    // order comments
    session.userData.comments = '';

    if (next) {
        next();
    }
}

function drinkStepSelectOther(session, args) {
    drinkResetDialogData(session);
    session.send('Thanks, waiter will contact you for more information');
}

// function registerReserveDrinksCallback(dialog) {
//     global.server.get('/reserve/drinks/:id/:userId', (req, res, next) => {
//         res.send('Hello')
//     });
// }

function drinkPrintReceipt(session, orders, comments) {

    let productsItems = [];

    let productItem = _.each(global.Config.products, (v) => {
        _.each(orders, (v2) => {
            if (v2.drinkId == v.id) {
                let p = _.clone(v);
                p.quantities = v2.drinkQuantities;
                //p.comments = v2.comments;

                productsItems.push(p);
            }
        })
        // return v.id == order.drinkId;
    });

    let productCardItem = [];
    let totalConst = 0;
    _.each(productsItems, (v) => {
        console.log(v);
        let productConst = v.price * parseInt(v.quantities);
        totalConst += productConst;
        productCardItem.push(new builder.ReceiptItem(session)
            //.price(`$${productConst}`)
            .subtitle(`£${v.price} x ${v.quantities}`)
            //.text(v.comments)
            .title(v.name)
            .image(new builder.CardImage(session).url(v.image)))
    });

    let vatRate = 10;
    let vatAmount = totalConst / 100 * vatRate;
    totalConst += vatAmount;

    let facts = [];
    if (_.isString(comments) && comments.length > 0) {
        facts.push(builder.Fact.create(session, comments, "Notes"));
    }

    var msg2 = new builder.Message(session)
        .attachments([
            new builder.ReceiptCard(session)
                .title("Drink Order Confirmation")
                .items(productCardItem)
                //.facts([

                //      builder.Fact.create(session, "VISA 4076", "Payment Method"),
                //      builder.Fact.create(session, "WILLCALL", "Delivery Method")
                //])
                .facts(facts)
                .buttons(
                [
                    new builder.CardAction(session)
                        .type('imBack')
                        .title('Confirm')
                        .value('Confirm'),
                    new builder.CardAction(session)
                        .type('imBack')
                        .title('Start Over')
                        .value('Start Over')
                ])
                .tax(`£${vatAmount.toFixed(2)}`)
                .total(`£${totalConst.toFixed(2)}`)
        ]);

    session.send(msg2);
    session.beginDialog('orderDrinkConfirmation');
}

function registerDialogSelectQuantities(bot, recognizer) {
    bot.dialog('orderDrinkSelectQuantities', [
        function (session) {
            let availableOptions = [
                'How many %s would you like to reserve?',
                'Let me know how many %s you’d like.'
            ];
            builder.Prompts.text(session, _.replace(_.sample(availableOptions), '%s', session.userData.currentOrder.drinkName));
        },
        function (session, results) {
            function repeatRequest(session) {
                session.send('Sorry, please enter the number only');
                //
                session.cancelDialog('*:orderDrinkSelectQuantities', 'orderDrinkSelectQuantities');
            }

            function tryParseQuantities(quantities) {
                if (parseInt(quantities) > 0) {
                    quantities = parseInt(quantities);
                    session.userData.currentOrder.drinkQuantities = quantities;

                    // inject current order into orders list
                    session.userData.orders = session.userData.orders || [];
                    session.userData.orders.push(_.clone(session.userData.currentOrder));

                    //session.beginDialog('orderDrinkComments');
                    session.beginDialog('orderMore');
                    return true;
                }
                return false;
            }

            if (results.response) {
                let quantities = builder.EntityRecognizer.parseNumber(results.response);

                if (!tryParseQuantities(quantities)) {
                    repeatRequest(session);
                }
            } else {
                repeatRequest(session);
            }
        }
    ]);
}

function registerDialogComments(bot, recognizer) {
    bot.dialog('orderDrinkComments', [
        function (session) {
            let availableOptions = [
                'Do you have any special comment?'
            ];
            builder.Prompts.text(session, _.sample(availableOptions));
        },
        function (session, results) {
            if (results.response) {
                if (session.userData.currentOrder.drinkId > 0 && session.userData.currentOrder.drinkQuantities > 0) {
                    // let's recognize it
                    recognizer.recognize({
                        message: {
                            text: results.response
                        },
                        dialogData: session.dialogData,
                        activeDialog: true
                    }, function (err, result) {
                        if (!err) {
                            if (result && result.intent
                                && result.intent == 'drink-no-comments' && result.score >= 0.9
                            ) {
                                //session.userData.currentOrder.comments = '';
                                session.userData.comments = '';
                            }
                            else {
                                //session.userData.currentOrder.comments = results.response;
                                session.userData.comments = results.response;
                            }

                            drinkPrintReceipt(session, _.clone(session.userData.orders), session.userData.comments);
                            //session.beginDialog('orderMore');
                        }
                        else {
                            throw err;
                        }
                    });
                }
                else {
                    session.send('Sorry, I dont understand for what you mean');
                    drinkResetDialogData(session);
                    session.endDialog();
                }
            } else {
                session.send("Sorry but we can not recognize what you mean");
                drinkResetDialogData(session);
                session.endDialog();
            }
        }
    ]);
}

function registerDialogMoreOrder(bot, recognizer) {
    bot.dialog('orderMore', [
        function (session) {
            let availableOptions = [
                'Would you like to order something else?'
            ];
            builder.Prompts.text(session, _.sample(availableOptions));
        },
        function (session, results) {
            if (results.response) {
                // let's recognize it
                recognizer.recognize({
                    message: {
                        text: results.response
                    },
                    dialogData: session.dialogData,
                    activeDialog: true
                }, function (err, result) {
                    if (!err) {
                        if (result && result.intent) {
                            if (result.intent == 'confirm-order' && result.score >= 0.5) {
                                // restart the dialog
                                //session.cancelDialog('*:drink-order', 'drink-order');
                                //
                                session.message.text = 'any drink';
                                session.beginDialog('drink-order');
                            }
                            else {
                                if (session.userData.orders.length > 0) {
                                    session.beginDialog('orderDrinkComments');
                                    //drinkPrintReceipt(session, _.clone(session.userData.orders));
                                }
                                else {
                                    session.send('Thank you. Good bye');
                                    session.userData.orders = [];
                                    session.userData.currentOrder = {};
                                    session.endConversation();
                                }
                            }
                        }
                        // drinkResetDialogData();
                        // session.endDialog();
                    }
                    else {
                        throw err;
                    }
                });
            } else {
                session.send("Sorry but we can not recognize what you mean");
                drinkResetDialogData(session);
                session.endDialog();
            }
        }
    ]);
}

function registerDialogOrderDrinkConfirmation(bot, recognizer) {
    bot.dialog('orderDrinkConfirmation', [
        function (session) {
            let availableOptions = [
                'That is what I have for you.',
                'Alright this is what I got so far, is this OK with you?',
                'Please let me know if this is OK.'
            ];
            builder.Prompts.text(session, _.sample(availableOptions));
        },
        function (session, results) {
            if (results.response) {
                // let's recognize it
                recognizer.recognize({
                    message: {
                        text: results.response
                    },
                    dialogData: session.dialogData,
                    activeDialog: true
                }, function (err, result) {
                    function stop() {
                        session.userData.orders = [];
                        session.userData.currentOrder = {};
                        session.endDialog();
                    }
                    if (!err) {
                        console.log(result);
                        if (result && result.intent) {
                            if (result.intent == 'confirm-order' && result.score >= 0.5) {
                                session.send('Noted. Your order has been received.');
                                // push the orders and comments into persistant memory
                                let userId = session.message.address.user.id;
                                global.DatabaseManager
                                    .saveOrder({
                                        name: session.message.user ? session.message.user.name : null,
                                        id: userId
                                    }, session.userData.orders, session.userData.comments)
                                    .then(() => {
                                        // clear all order
                                        session.userData.orders = [];
                                        session.userData.currentOrder = {};
                                        //
                                        session.beginDialog('orderMore');
                                    });
                            }
                            else if (result.intent == 'drink-no-comments' && result.score >= 0.5) {
                                session.send('Thank you');
                                stop();
                            }
                            else if (result.intent == 'start-over' && result.score >= 0.8) {
                                session.send('Thank you. Good bye');
                                stop();
                            }
                        }
                        // drinkResetDialogData();
                        // session.endDialog();
                    }
                    else {
                        throw err;
                    }
                });
            } else {
                session.send("Sorry but we can not recognize what you mean");
                drinkResetDialogData(session);
                session.endDialog();
            }
        }
    ]);
}

function scenarioDrinkOrder(recognizer, bot, mainDialog) {
    let dialog = new builder.IntentDialog({
        recognizers: [recognizer]
        , recognizeMode: builder.RecognizeMode.onBegin
    });
    dialog['_internalName'] = 'scenarioDrinkOrder';
    // registerReserveDrinksCallback(dialog);

    mainDialog.matches('drink-select', drinkStepSelect);

    dialog
        .matches('order-drink', drinkStepShowCategories)
        .matches('drink-select', drinkStepSelect)
        .matches('drink-other', drinkStepSelectOther)
        .onBegin(drinkResetDialogData)
        .onDefault(function (session) {
            if (session.message.text.length > 0) {
                session.send("I dont understand for what you mean");
                session.endConversation();
            }
        });

    bot.dialog('drink-order', dialog);
    registerDialogSelectQuantities(bot, recognizer);
    registerDialogComments(bot, recognizer);
    registerDialogOrderDrinkConfirmation(bot, recognizer);
    registerDialogMoreOrder(bot, recognizer);
}

module.exports = scenarioDrinkOrder;