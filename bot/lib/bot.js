/**
 * Bot wrapper
 * 
 * NashTech VietNam
 * 
 * Copyright @ 2016 Trung Dang
 */
'use strict';

const restify = require('restify');
const builder = require('botbuilder');

/**
* Init RESTful server to work as endpoint of Skype 
* 
* @returns {restify}
*/
function initRestServer(config) {
    let server = restify.createServer();
    server.listen(process.env.port || process.env.PORT || config.PORT || 3981, function () {
        console.log('%s listening to %s', server.name, server.url);
    });
    return server;
}

function initConsoleBot() {
    let connector = new builder.ConsoleConnector().listen();
    return new builder.UniversalBot(connector);
}

function initLuisBot(config) {
    console.log(`Server is starting in ${process.env.NODE_ENV} with ngrok id = ${process.env.NGROK_ID}`);
    let server = initRestServer(config);

    // Create chat bot
    var connector = new builder.ChatConnector({
        appId: config.API_ID,
        appPassword: config.API_PASSWORD,
        // callbackUrl: config.CALLBACK_URL
    });

    let bot = new builder.UniversalBot(connector, {
        autoBatchDelay: 10
    });
    server.post(config.CHAT_ENDPOINT_URI, connector.listen());

    server.get('/orders', (req, res, next) => {
        global.DatabaseManager.loadOrders()
            .then(orders => {
                res.send(orders);
            });
    });

    global.server = server;
    return bot;
}

/**
 * Init an instance of Bot
 * Currently support: CHAT
 * 
 * @param {any} config
 * @returns
 */
function initBot(config) {
    let bot;
    if (config.runMode && config.runMode === 'console') {
        bot = initConsoleBot();
    }
    else {
        bot = initLuisBot(config);
    }

    global.Config = config;

    return bot;
}

module.exports = initBot;