'use strict';

const _ = require('lodash');
let knex;

class Database {
    constructor() {
        knex = require('knex')({
            client: 'sqlite3',
            connection: {
                filename: 'mydb.sqlite'
            }
        });

        knex.schema
            .createTableIfNotExists("orders", function (table) {
                table.increments(); // integer id

                // name
                table.string('name');

                table.string('userGUID');

                // comments
                table.string('comments');

                table.dateTime('createdOn');
            })
            .catch((err) => {
                console.log(err);
            });

        knex.schema.createTableIfNotExists("orderdetail", function (table) {
            table.increments(); // integer id

            // name
            table.integer('orderId');

            table.integer('productId');

            table.string('productName');

            table.integer('quantities');
        }).catch((err) => {
            console.log(err);
        });

    }

    saveOrder(user, orders, comments) {
        return knex('orders')
            .returning('id')
            .insert({
                name: user.name,
                userGUID: user.id,
                comments: comments,
                createdOn: Date.now()
            }).then(id => {
                if (id.length == 0) {
                    throw new Error('Could not save data into database');
                }
                let orderId = id[0];
                return orderId;
            }).then(orderId => {
                let data = [];

                _.each(orders, (v) => {
                    data.push({
                        orderId: orderId,
                        productId: v.drinkId,
                        productName: v.drinkName,
                        quantities: v.drinkQuantities
                    });
                });

                return knex('orderdetail')
                    .returning('id')
                    .insert(data);
            });
    }

    loadOrders() {
        return Promise.all([
            knex.select('*').from('orders'),
            knex.select('*').from('orderdetail')
        ])
            .then(data => {
                let orders = data[0];
                let orderDetail = data[1];
                let ret = [];

                _.each(orders, (v) => {
                    ret.push(_.merge({}, v, {
                        products: _.filter(orderDetail, (k) => {
                            return k.orderId === v.id
                        })
                    }));
                });

                return ret;
            })
    }
}

module.exports = function exportDatabase() {
    console.log('init database ....');
    global.DatabaseManager = new Database();
    return global.DatabaseManager;
} 