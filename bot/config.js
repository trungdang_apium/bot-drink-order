'use strict';
const _ = require('lodash');

let productCategories = [
    {
        id: 1,
        name: 'Beer',
        desc: 'Saigon, 333, Tiger, Heineken, Guinness',
        image: 'https://httpsimage.com/img/web1_beer-tasting.1_2.jpg',
        regex: /^(?=.*\bbeer\b).*$/i,
        imBack: 'I want Beer'
    },
    {
        id: 2,
        name: 'Cocktails',
        desc: 'Mojito, Sunrise, Cosmopolitan, Angel Face, Rising Sun, ...',
        image: 'https://httpsimage.com/img/summer-cocktails-24374-3.jpg',
        regex: /^(?=.*\bCocktail\b).*$/i,
        imBack: 'I want Cocktails'
    },
    {
        id: 3,
        name: 'Wine',
        desc: 'Red, White',
        image: 'https://httpsimage.com/img/vino.jpg',
        regex: /^(?=.*\bWine\b).*$/i,
        imBack: 'I want Wine'
    },
    {
        id: 4,
        name: 'Whisky',
        desc: 'Macallan Sherry Oak 18, Chivas Royal Salute 18',
        image: 'https://httpsimage.com/img/whisky-jack-e1-8659952b79f745d9600d5a39a6c98176-1024-1024-600x600.jpg',
        regex: /^(?=.*\bWhisky\b).*$/i,
        imBack: 'I want Whisky'
    },
    {
        id: 5,
        name: 'Soft Drinks',
        desc: 'Juice, Soda',
        image: 'https://httpsimage.com/img/Soft-drinks-and-beverages-to-diabetes.jpg',
        regex: /^(?=.*\bsoft\b|.*\bdrink\b).*$/i,
        imBack: 'I want Soft Drinks'
    }
];

let products = [];
let ngrokId = process.env.NGROK_ID || 'd3257be3';
let config = {
    'development': {
        API_ID: '117dee3e-1f8d-474d-92cd-48d9625b290e',
        API_PASSWORD: '1Lmtdj2SruJRTm71re4Dint',
        CALLBACK_URL: `https://${process.env.NGROK_ID}.ngrok.io/`,
        RESERVE_DRINKS_URL: `https://${process.env.NGROK_ID}.ngrok.io/reserve/drinks`,
        LUIS_SERVICE_URL: 'https://api.projectoxford.ai/luis/v1/application?id=cbd28606-a6a0-44a6-9ec7-bb41e11ba19e&subscription-key=913523e48be2468b8d3b96dfd7487ad2',
        CALL_ENDPOINT_URI: '/api/calls',
        CHAT_ENDPOINT_URI: '/',
        //?id=11112312&type=skype
        AUTHENTICATE_URL: 'https://127.0.0.1:3000/auth.html#/bot_auth',

        dbPath: "../database/apiServer.sqlite",
        PORT: 3035,
        runMode: 'skype',
        enableCalling: true,
        products: products,
        productCategories: productCategories
    },
    'production': {
        API_ID: 'e6615a05-fded-486a-a986-fb8f84fd8f9d',
        API_PASSWORD: 'KpofNeZZqvarPuvDP2OmAok',
        CALLBACK_URL: `https://${process.env.NGROK_ID}.ngrok.io/`,
        RESERVE_DRINKS_URL: `https://${process.env.NGROK_ID}.ngrok.io/reserve/drinks`,
        LUIS_SERVICE_URL: 'https://api.projectoxford.ai/luis/v1/application?id=cbd28606-a6a0-44a6-9ec7-bb41e11ba19e&subscription-key=913523e48be2468b8d3b96dfd7487ad2',
        CALL_ENDPOINT_URI: '',
        CHAT_ENDPOINT_URI: '/',
        //?id=11112312&type=skype
        AUTHENTICATE_URL: 'https://128.199.252.140/auth.html#/bot_auth',

        dbPath: "../database/apiServer.sqlite",
        PORT: 3035,
        runMode: 'skype',
        enableCalling: false,
        products: products,
        productCategories: productCategories
    }
};

function addProduct(catId, product) {
    // find maxId
    let maxId = _.max(_.map(products, (v) => {
        return v.id
    }));
    // push to products
    products.push(_.merge(product, {
        catId: catId,
        id: (maxId > 0) ? maxId + 1 : 1
    }));
}

function initCocktails() {
    let catId = 2;
    addProduct(catId, {
        name: 'Mojito',
        desc: 'Brugal Light Rum, fresh mint, brown sugar and lime',
        image: 'https://httpsimage.com/img/mojito-3-300x300.jpg',
        imBack: 'I want Mojito',
        regex: /^(?=.*\bmojito\b).*$/i,
        price: 10.5
    });
    addProduct(catId, {
        name: 'Cosmopolitan',
        desc: 'Belverdere Vodka, Grand Marnier, cranberry juice and fresh lime',
        image: 'https://httpsimage.com/img/cosmopolitan-drink1-300x200.jpg',
        imBack: 'I want Cosmopolitan',
        regex: /^(?=.*\bcosmopolitan\b).*$/i,
        price: 4.5
    });
    addProduct(catId, {
        name: 'Angel Face',
        desc: 'Bombay Sapphire Gin, Calvados and Apricot Brandy',
        image: 'https://httpsimage.com/img/37_c_angel-face-walter-pintus-rivoli-bar-ritz-london-pm.jpg',
        imBack: 'I want Angel Face',
        regex: /^(?=.*\bangle face\b).*$/i,
        price: 5
    });
    addProduct(catId, {
        name: 'Rising Sun',
        desc: 'Belvedere Vodka, passion purée, simple syrup and spicy mix',
        image: 'https://httpsimage.com/img/raising sun.jpg',
        imBack: 'Rising Sun, please',
        regex: /^(?=.*\brising sun\b).*$/i,
        price: 6
    });
    addProduct(catId, {
        name: 'Other drinks',
        desc: '',
        image: 'https://httpsimage.com/img/8f81be50b639e07751958a9df84bcb99.jpg',
        imBack: 'I want other drinks',
        price: 0
    });
}

function initBeer() {
    // Saigon, 333, Tiger, Heineken, Guinness
    let catId = 1;
    addProduct(catId, {
        name: 'Saigon',
        desc: 'Beer Saigon Xanh',
        image: 'https://httpsimage.com/img/sg_xanh(1).jpg',
        imBack: 'I want Saigon',
        regex: /^(?=.*\bsaigon\b).*$/i,
        price: 0.54
    });
    addProduct(catId, {
        name: '333',
        desc: 'Beer 333 - Vietnamese beer',
        image: 'https://httpsimage.com/img/1369212588bia_333(1).jpg',
        imBack: 'I want 333',
        regex: /^(?=.*\b333\b).*$/i,
        price: 0.32
    });
    addProduct(catId, {
        name: 'Tiger',
        desc: 'Tiger beer',
        image: 'https://httpsimage.com/img/1450324227_tiger2.jpg',
        imBack: 'I want Tiger beer',
        regex: /^(?=.*\btiger\b).*$/i,
        price: 0.54
    });
    addProduct(catId, {
        name: 'Heineken',
        desc: 'Heineken beer',
        image: 'https://httpsimage.com/img/bia-heineken-chai-250ml-nắp-vặn.jpg',
        imBack: 'I want Heineken beer',
        regex: /^(?=.*\bheineken\b).*$/i,
        price: 0.65
    });
    addProduct(catId, {
        name: 'Guinness',
        desc: 'Guinness beer',
        image: 'https://s-media-cache-ak0.pinimg.com/originals/57/f5/b1/57f5b19348fcab4c5afc835f39fe2e4f.jpg',
        imBack: 'I want Guinness beer',
        regex: /^(?=.*\bguinness\b).*$/i,
        price: 0.79
    });
}

function initWine() {
    let catId = 3;
    addProduct(catId, {
        name: 'Red Wine',
        desc: 'Red wine is a type of wine made from dark-colored (black) grape varieties.',
        image: 'https://httpsimage.com/img/red_wine5.jpg',
        imBack: 'I want Red wine',
        regex: /^(?=.*\bred wine\b).*$/i,
        price: 2
    });

    addProduct(catId, {
        name: 'White Wine',
        desc: 'White wine is a wine whose color can be straw-yellow, yellow-green, or yellow-gold coloured.',
        image: 'https://httpsimage.com/img/White-Wine.jpg',
        imBack: 'I want White wine',
        regex: /^(?=.*\bwhite wine\b).*$/i,
        price: 1.75
    });
}

function initWhisky() {
    let catId = 4;

    addProduct(catId, {
        name: 'Macallan Sherry Oak 18',
        desc: 'The Macallan 18 year old has a light mahogany colour and an aroma of dried fruits and ginger, providing a full, lingering finish.',
        image: 'https://img.thewhiskyexchange.com/540/macob.1994.jpg',
        imBack: 'I want Macallan Sherry Oak 18',
        regex: /^(?=.*\bmacallan\b|.*\bSherry\b|.*\boak 18\b).*$/igm,
        price: 21
    });

    addProduct(catId, {
        name: 'Chivas Royal Salute 18',
        desc: 'The Chivas Regal 18 Year Old was personally created by Master Blender Colin',
        image: 'https://httpsimage.com/img/chivas18-dd.jpg',
        imBack: 'I want Chivas Royal Salute 18',
        regex: /^(?=.*\bchivas\b|.*\broyal\b|.*\bsalute\b).*$/igm,
        price: 20
    });
}

function initSoftDrinks() {
    let catId = 5;

    addProduct(catId, {
        name: 'Juice',
        desc: 'Good for your health',
        image: 'https://httpsimage.com/img/healthy-fruit-juices.jpg',
        imBack: 'I want Juice',
        regex: /^(?=.*\bjuice\b).*$/i,
        price: 0.86
    });

    addProduct(catId, {
        name: 'Soda',
        desc: 'All of your soda flavors',
        image: 'https://httpsimage.com/img/SqueezeEzy-Flavour-3-small11.png',
        imBack: 'I want Soda',
        regex: /^(?=.*\bsoda\b).*$/i,
        price: 0.95
    });
}

initBeer();
initCocktails();
initWine();
initWhisky();
initSoftDrinks();

process.env.NODE_ENV = process.env.NODE_ENV || "development";
module.exports = (process.env.NODE_ENV == 'production') ? config.production : config.development;